CREATE EXTENSION IF NOT EXISTS postgis;

--Dodana tabela 'plan' sluzaca do grupowania projektow w obrebie jednego duzego projektu
CREATE TABLE plan (
	id text PRIMARY KEY
);

--Zmienna planid nie wystepuje w json - sluzy do grupowania projektow w jeden duzy projekt

CREATE TABLE project (
    vendor text,
    name text PRIMARY KEY,
	planid text REFERENCES plan(id) ON DELETE CASCADE,
    date text,
    version integer DEFAULT 1,
    comment text
);

CREATE TABLE roadsurface (
    id text PRIMARY KEY,
    name text,
    q0 real,
    s1 real,
    q0wet real
);

--Zdublowane Emin - jedno z nich zastapiono eavgmin

CREATE TABLE lightingclass (
    id text PRIMARY KEY,
    l numeric(20,10),
    u0 numeric(20,10),
    ui numeric(20,10),
    tl numeric(20,10),
    sr numeric(20,10),
	eavgmin numeric(20,10),
    emin numeric(20,10),
    uo numeric(20,10),
    ehs numeric(20,10),
    u0minhs numeric(20,10),
    escmin numeric(20,10),
    evmin numeric(20,10)
);

--TODO pole - do polaczenia

CREATE TABLE pole (
    id text PRIMARY KEY,
    cabinet text,
    coordinates geography(POINT),
    elev real,
    poleheight real,
    nextpoleid text REFERENCES pole(id) ON DELETE SET NULL
);

CREATE TABLE poledistances (
    poleid text REFERENCES pole(id) ON DELETE CASCADE,
    neighboringpoleid text REFERENCES pole(id) ON DELETE CASCADE,
    distance real
);

-- dodane projid

CREATE TABLE roadsegment (
    id text PRIMARY KEY,
	projid text REFERENCES project(name) ON DELETE CASCADE,
    street text,
    "desc" text,
    mainlightingclass text REFERENCES lightingclass(id) ON DELETE SET NULL,
    lamparrangement text
);

CREATE TABLE roadsegmentcoordinates (
    roadsegmentid text REFERENCES roadsegment(id) ON DELETE CASCADE,
    coordinates geography(POINT),
    elev real,
    "order" integer,
    "group" integer
);

CREATE TABLE roadsection (
    id text PRIMARY KEY,
    roadsegmentid text REFERENCES roadsegment(id) ON DELETE CASCADE,
    idx integer,
    type text,
    widthstart real,
    widthend real,
    elevationstart real,
    elevationend real,
    numberoflanes integer,
    roadsurfaceid text REFERENCES roadsurface(id) ON DELETE SET NULL,
    lightingclassid text REFERENCES lightingclass(id) ON DELETE SET NULL
);

CREATE TABLE roadsectionmeasurement (
    poleid text REFERENCES pole(id) ON DELETE CASCADE,
    roadsectionitemid text REFERENCES roadsection(id) ON DELETE CASCADE,
    width real,
    elevation real,
    numberoflanes integer
);

CREATE TABLE influences (
    poleid text REFERENCES pole(id) ON DELETE CASCADE,
    segmentid text REFERENCES roadsegment(id),
    roadsectionitemid text REFERENCES roadsection(id) ON DELETE CASCADE
);

CREATE TABLE doesntinfluence (
    poleid  text REFERENCES pole(id) ON DELETE CASCADE,
    segmentid text REFERENCES roadsegment(id) ON DELETE CASCADE,
    roadsectionitemid text REFERENCES roadsection(id) ON DELETE CASCADE
);

CREATE TABLE fixturetype (
    id text PRIMARY KEY,
    model text,
    ldtprofilefile text,
    totalluminousflux integer,
    totalwattage integer
);

-- Dodano sztuczny klucz id

CREATE TABLE arm (
    id serial PRIMARY KEY,
    poleid text REFERENCES pole(id) ON DELETE CASCADE,
    overhang real,
    armmountingheight real,
    armlength real,
    armangle real,
    armazimuth real
);

CREATE TABLE fixture (
    id text PRIMARY KEY,
    armid integer REFERENCES arm(id) ON DELETE CASCADE,
    azimuth real,
    inclination real,
    rotation real,
    mountingheight real,
    fixturetypeid text REFERENCES fixturetype(id) ON DELETE SET NULL,
    maintenancefactor real,
    luminousfluxratio real
);

--Zdublowane Emin - jedno z nich zastapiono eavgmin
-- przyjeto ze calcid jest glownym calcid

CREATE TABLE projectcalculation (
    calcid text PRIMARY KEY,
    roadsegmentid text REFERENCES roadsegment(id) ON DELETE CASCADE,
    projid text REFERENCES project(name) ON DELETE CASCADE,
    roadsectionid text REFERENCES roadsection(id) ON DELETE CASCADE,
    l numeric(20,10),
    u0 numeric(20,10),
    ui numeric(20,10),
    tl numeric(20,10),
    sr numeric(20,10),
	eavgmin numeric(20,10),
    emin numeric(20,10),
    uo numeric(20,10),
    ehs numeric(20,10),
    u0minhs numeric(20,10),
    escmin numeric(20,10),
    evmin numeric(20,10),
    horizfile text,
    horizmin numeric(20,10),
    horizavg numeric(20,10),
    horizmax numeric(20,10),
    vertfile text,
    vertmin numeric(20,10),
    vertavg numeric(20,10),
    vertmax numeric(20,10),
    hemifile text,
    hemimin numeric(20,10),
    hemiavg numeric(20,10),
    hemimax numeric(20,10),
    semifile text,
    semimin numeric(20,10),
    semiavg numeric(20,10),
    semimax numeric(20,10),
    calculationinputid text UNIQUE,
    fixturetypeid text REFERENCES fixturetype(id) ON DELETE SET NULL,
    fixtureid text REFERENCES fixture(id) ON DELETE CASCADE,
    lfr real,
    lampspace real,
    mountingheight real,
    tilt real,
    azmiuth real,
    rotation real,
    arm real,
    setback real,
    totalpower real
);

CREATE TABLE fixturelist (
    calcid text REFERENCES projectcalculation(calcid) ON DELETE CASCADE,
    fixtureid text REFERENCES fixture(id) ON DELETE CASCADE
);

--Dodano sztuczny klucz id

CREATE TABLE lampseries (
    id serial PRIMARY KEY,
    roadsegmentid text REFERENCES roadsegment(id) ON DELETE CASCADE,
    roadsectionid text REFERENCES roadsection(id) ON DELETE CASCADE,
    lampcount integer,
    space real,
    avgmountingheight real,
    avginclination real,
    avgazimuth real,
    avgrotation real,
    avgarmlength real,
    s real,
    avgoverhang real,
    avgmf real,
    side text,
    avgcf real,
    avgoffset real
);

--Dodano sztuczny klucz id

CREATE TABLE lampseriespoles (
	id serial PRIMARY KEY,
    poleid text REFERENCES pole(id) ON DELETE CASCADE,
    lampseriesid integer REFERENCES lampseries(id) ON DELETE CASCADE
);

CREATE TABLE lightingprofile (
    id text PRIMARY KEY,
    lightingclassid text REFERENCES lightingclass(id) ON DELETE CASCADE,
    ambientfrom integer,
    ambientto integer
);

--Dodano sztuczny klucz id

CREATE TABLE lightingsegment (
    id serial PRIMARY KEY,
    roadsectionid text REFERENCES roadsection(id) ON DELETE CASCADE,
    roadsegmentid text REFERENCES roadsegment(id) ON DELETE CASCADE,
    lightingclassid text REFERENCES lightingclass(id) ON DELETE SET NULL,
    comment text
);

CREATE TABLE lightingsegmentcoordinates (
    lightingsegmentid integer REFERENCES lightingsegment(id) ON DELETE CASCADE,
    coordinates geography(POINT),
    elev real
);

CREATE TABLE lightpoints (
    calcid text REFERENCES projectcalculation(calcid) ON DELETE CASCADE,
    fixtureid text REFERENCES fixture(id) ON DELETE CASCADE,
    lumenfluxratio real
);

--UWAGA calcid odwoluje sie do projectcalculation(calculationinputid) - zgodnie z JSON 1.3

CREATE TABLE projectcalculationinputrange (
    calcid text REFERENCES projectcalculation(calculationinputid) ON DELETE CASCADE,
    projid text REFERENCES project(name) ON DELETE CASCADE,
    overmin real,
    overmax real,
    overstep real,
    lampmin real,
    lampmax real,
    lampstep real,
    mountmin real,
    mountmax real,
    mountstep real,
    lumenmin real,
    lumenmax real,
    lumenstep real,
    tiltmin real,
    tiltmax real,
    tiltstep real,
    azmiuthmin real,
    azimuthmax real,
    azimuthstep real,
    rotationmin real,
    rotationmax real,
    rotationstep real,
    armmin real,
    armmax real,
    armstep real,
    setbackmin real,
    setbackmax real,
    setbackstep real,
    mf real,
    cf real
);

--TODO z czym polaczyc

CREATE TABLE projectveryficationvector (
    id text PRIMARY KEY,
    mainlightingclass text REFERENCES lightingclass(id) ON DELETE SET NULL
);

CREATE TABLE projectfixtureconfiguration (
    calcid text REFERENCES projectcalculation(calcid) ON DELETE CASCADE,
    lightingprofileid text REFERENCES lightingprofile(id) ON DELETE SET NULL,
    roadsegmentid text REFERENCES roadsegment(id) ON DELETE CASCADE,
    fixturetypeid text REFERENCES fixturetype(id) ON DELETE SET NULL,
    lumenfluxratio real,
    lightingsegmentid integer REFERENCES lightingsegment(id) ON DELETE CASCADE
);

CREATE TABLE projectveryfication (
    projectveryficationvectorid text REFERENCES projectveryficationvector(id) ON DELETE CASCADE,
    lightingprofileid text REFERENCES lightingprofile(id) ON DELETE SET NULL,
    weight real
);

CREATE TABLE projvervectorinput (
    roadsegmentid text REFERENCES roadsegment(id) ON DELETE CASCADE,
    projectveryficationvectorid text REFERENCES projectveryficationvector(id) ON DELETE CASCADE
);

CREATE TABLE roadassigments (
    poleid text REFERENCES pole(id) ON DELETE CASCADE,
    setback real,
    roadsegmentid text REFERENCES roadsegment(id) ON DELETE CASCADE,
    roadsectionitemid1 text REFERENCES roadsection(id) ON DELETE CASCADE,
    roadsectionitemid2 text REFERENCES roadsection(id) ON DELETE CASCADE
);

CREATE TABLE segmentarray (
    calcid text REFERENCES projectcalculation(calcid) ON DELETE CASCADE,
    roadsegmentid text REFERENCES roadsegment(id) ON DELETE CASCADE,
    lightingsegmentid integer REFERENCES lightingsegment(id) ON DELETE CASCADE
);
